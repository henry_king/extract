@ My intention is to have this code freely available,
I hope the following will accomplish this.

@<GNU license@>=

/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


@ Main structure
@c
@<GNU license@>@;
@<globals, etc.@>@;
@<basic vector operations@>@;
@<utility functions@>@;
@<display functions@>@;
@<callback functions@>@;
@<main functions@>@;

@ You should initialize GLUT by calling the following,
with the window name.
@<main functions@>=
void init_glut_window(char *vfilename)
{
	glutInitWindowSize(winWidth = 512, winHeight = 512);
	glutInitDisplayMode (GLUT_DEPTH | GLUT_SINGLE | GLUT_RGB);
	
	glutCreateWindow(vfilename);
	
	@<tell GLUT about our callback functions@>@;
	
    @<create menu@>@;
    
    @<find range of x,y, and z coordinates@>@;
    
    @<determine reasonable display size for critical simplices@>@;
    
    @<determine an initial viewpoint@>@;
    
    @<set up lighting@>@;
    
	@<set up initial persistence for cancelations@>@;
	
	@<indicate display should change@>@;
}

@ After initialization you need to turn to GLUT
to handle user interactions.
@<main functions@>=
void
begin_glut()
{	
	glutMainLoop();
}

@ @<indicate display should change@>=
	glutPostRedisplay();





@ @<tell GLUT about our callback functions@>=
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMotionFunc(motion);
    glutMouseFunc(mouse);
    glutReshapeFunc(reshape);

@ @<create menu@>=
	menu = glutCreateMenu(menu_hit);
	glutAddMenuEntry("Togggle Display 0-1 gradient paths", 1);
	glutAddMenuEntry("Togggle Display 1-2 gradient paths", 2);
	glutAddMenuEntry("Togggle Display 2-3 gradient paths", 3);
	glutAddMenuEntry("Togggle Display descending 2 discs", 4);
	glutAddMenuEntry("Togggle Display link", 5);
	glutAddMenuEntry("Togggle Display lower link", 6);
	glutAddMenuEntry("Cancel 0-1", 7);
	glutAddMenuEntry("Cancel 1-2", 8);
	glutAddMenuEntry("Cancel 2-3", 9);
	glutAddMenuEntry("Compute Z/pZ Betti numbers", 10);
	glutAddMenuEntry("Togggle Display boundary", 11);
	glutAddMenuEntry("Togggle set option", 12);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

@ @<find range of x,y, and z coordinates@>=
{
	int32_t i,j;
	vertex *v;
	GLfloat *p;

	for(i=0;i<number_of_vertices;i++)
	{
		v = vertexlist+i;
		if((v->type & 1)==0) continue;
		p = v->coords;
		for(j=0;j<3;j++)
		{
			if(i==0)  mins[j]=maxs[j]=p[j];
			else if(p[j]<mins[j]) mins[j]=p[j];
			else if(p[j]>maxs[j]) maxs[j]=p[j];
		}
	}
	
	scale = maxs[0]-mins[0];
	if(scale <maxs[1]-mins[1]) scale = maxs[1]-mins[1];
	if(scale <maxs[2]-mins[2]) scale = maxs[2]-mins[2];
	
	for(j=0;j<3;j++)
	{
		mins[j] = (mins[j]+maxs[j]-scale)/2;
		maxs[j] = mins[j]+scale;
	}

// fix various scaling distances
	scalemove1 = scale*.3;
	scalemove = scale*.01;
	scale_epsilon = scale*.00001;
}

@ @<determine reasonable display size for critical simplices@>=
{
	GLfloat *p,*q,c[3],a[3];
	simplex_id s;
	GLfloat z,w=scale;
	int i;

	list_read_init(crit[1]);
	while (!id_is_null(s=id_list_read(crit[1])))  // run through all critical edges
	{
		p = id2vertex(get_vertex(s,0))->coords;     
		q = id2vertex(get_vertex(s,1))->coords;   
		z = distancev(p,q)*.65;
		if(z<w) w=z;  
	}
	
	list_read_init(crit[2]);
	while (!id_is_null(s=id_list_read(crit[2])))  // run through all critical faces
	{
		barycenter(s,c); 
		for(i=0;i<3;i++)
		{
			barycenter(get_edge(s,i),a); 
			z = distancev(a,c)*2.4;
			if(z<w) w=z; 
		} 
	}
	
	list_read_init(crit[3]);
	while (!id_is_null(s=id_list_read(crit[3])))  // run through all critical tetra
	{
		barycenter(s,c); 
		for(i=0;i<4;i++)
		{
			barycenter(get_face(s,i),a); 
			z = distancev(a,c)*3.2;
			if(z<w) w=z; 
		} 
	}
	
	
	scalecrit0 = w;
	scalecrit1 = w/2;
	scalecrit2 = w/3;
	scalecrit3 = w/4;

}

@ @<determine an initial viewpoint@>=
{
	GLfloat *p;
	int i;
	
	// start with identity
	for(i=1;i<15;i++) model_view_matrix[i]=0.0;
	for(i=0;i<16;i+=5) model_view_matrix[i]=1.0;
	
	// and look at a critical vertex

	p = id2vertex(*((simplex_id *)list_entry(crit[0],0)))->coords;
	posx = p[0]+scalemove1*headx;
	posy = p[1]+scalemove1*heady;
	posz = p[2]+scalemove1*headz;
}


@ @<set up lighting@>=
{
	GLfloat light_position[4]; 
	GLfloat light_ambient[] = { .3, .3, .3, 0.0 }; 
	
	glClearColor(0.0, 0.0, 0.0, 0.0); 
	
	/* The first light is somewhat to the right and above our viewpoint.
	 It grows less intense with distance. */
	light_position[0] = 0.2*scale;
	light_position[1] = 0.2*scale;
	light_position[2] = 0.0;
	light_position[3] = 1.0;
	glLightfv(GL_LIGHT0, GL_POSITION, light_position); 
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 1.0); 

	// The second light is somewhere at infinity.
	light_position[0] = maxs[0]+scale/3;
	light_position[1] = maxs[1]+scale/3;
	light_position[2] = maxs[2]+scale/3;
	light_position[3] = 0.0;
	glLightfv(GL_LIGHT1, GL_POSITION, light_position); 
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient); 

	
   	glShadeModel (GL_SMOOTH);  	
	glEnable(GL_LIGHTING);    
	glEnable(GL_LIGHT0); 	
	glEnable(GL_LIGHT1); 	
	glEnable(GL_DEPTH_TEST);
	
	// There is also a general fog which obscures distant objects.
	glEnable(GL_FOG);   
	{       
		GLfloat fogColor[4] = {0.0, 0.0, 0.0, 1.0};       
		glFogi (GL_FOG_MODE, GL_LINEAR);       
		glFogfv (GL_FOG_COLOR, fogColor);       
		glFogf (GL_FOG_DENSITY, 1.5/scale);       
		glHint (GL_FOG_HINT, GL_DONT_CARE);       
		glFogf (GL_FOG_START, .20*scale);       
		glFogf (GL_FOG_END, 3*scale);   
	}  
	
}

@ @<set up initial persistence for cancelations@>=
	cancel01 = cancel12 = cancel23 = global_persist;


@ This is where we go when a menu item is selected.
@<callback functions@>+=
void 
menu_hit(int item)
{
	switch(item)
	{
		case 1:
			@<respond to Display 0-1 gradient paths menu item@>@;
			break;
		case 2:
			@<respond to Display 1-2 gradient paths menu item@>@;
			break;
		case 3:
			@<respond to Display 2-3 gradient paths menu item@>@;
			break;
		case 4:
			@<respond to Display descending 2 discs menu item@>@;
			break;
		case 5:
			@<respond to Display links menu item@>@;
			break;
		case 6:
			@<respond to Display lower links menu item@>@;
			break;
		case 7:
			@<respond to Cancel 0-1 menu item@>@;
			break;
		case 8:
			@<respond to Cancel 1-2 menu item@>@;
			break;
		case 9:
			@<respond to Cancel 2-3 menu item@>@;
			break;
		case 10:
			test_homology(0xffff);
			break;
		case 11:
			@<respond to Display boundary menu item@>@;
			break;
		case 12:
			@<respond to set option menu item@>@;
			break;
	}
	if(item!=10)
		@<indicate display should change@>@;
}

@	@<respond to Display 0-1 gradient paths menu item@>=
	disp01 ^= 1;


@	@<respond to Display 1-2 gradient paths menu item@>=
	disp12 ^= 1;


@	@<respond to Display 2-3 gradient paths menu item@>=
	disp23 ^= 1;


@	@<respond to Display descending 2 discs menu item@>=
	dispd ^= 1;


@	@<respond to Display links menu item@>=
	if(displ>0) 
	{
		displ=0;
	}
	else
	{
		displ=1;
	}


@	@<respond to Display lower links menu item@>=
	if(displ<0) 
	{
		displ=0;
	}
	else
	{
		displ=-1;
	}


@	@<respond to Cancel 0-1 menu item@>=
	cancel01*=2;
	ExtractCancel1(cancel01);
	@<print new numbers of critical points@>@;


@ 	@<respond to Cancel 1-2 menu item@>=
	cancel12*=2;
	ExtractCancel2(cancel12);
	@<print new numbers of critical points@>@;


@ 	@<respond to Cancel 2-3 menu item@>=
	cancel23*=2;
	ExtractCancel3(cancel23);
	@<print new numbers of critical points@>@;
	
@ @<print new numbers of critical points@>=
	clean_crit();
	printf("\n Number of critical simplices = %d, %d, %d, %d\n",
		list_count(crit[0]),list_count(crit[1]),
		list_count(crit[2]),list_count(crit[3]));
	abandon_lists();


@ @<respond to Display boundary menu item@>=
	dispb ^= 1;


@ @<respond to set option menu item@>=
	disp_option ^= 1;
	
@ Here is where we display everything.
@<callback functions@>+=
void
display(void)
{
	int i;
	GLfloat c[3];
	vertex *vv;
	char num_str[128];
	GLfloat mat_emmission[] = { 0.5, .5, .5, 1.0 }; 
	GLfloat mat_diffuse[]= {0.5,.5,.5,1.0};

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 	
	glColor3f (1.0, 1.0, 1.0);  
	
	@<set up model view@>@;
	
	
	@<display all vertices@>@;

	@<display critical simplices@>@;

	if(disp01 && !id_is_null(selected_simplex))
		@<display 0-1 gradient paths@>@;
	
	if(disp12 && !id_is_null(selected_simplex))
		@<display 1-2 gradient paths@>@;
	
	
	if(disp23 && !id_is_null(selected_simplex))
		@<display 2-3 gradient paths@>@;
	
	if(dispd && !id_is_null(selected_simplex))
		@<display descending/ascending discs@>@;
	
	if(displ && !id_is_null(selected_simplex))
		@<display link@>@;
	
	if(dispb) display_boundary();
	
	@<display some info in the corner@>@;
	
	glFlush (); 
}

@ @<display all vertices@>=
    glBegin(GL_POINTS); 
		for(i=0,vv=vertexlist;i<number_of_vertices;i++,vv++) 
		{
			if(vv->type&1)
				glVertex3fv(vv->coords); 
		}
	glEnd(); 

@	@<display critical simplices@>=
	display_crit0(GL_RENDER);
	display_crit1(GL_RENDER);
	display_crit2(GL_RENDER);
	display_crit3(GL_RENDER);

@	@<display 0-1 gradient paths@>=
{
	if(id_dimension(selected_simplex)==0) 
	{
		display_grad_to_v(selected_simplex);
	}
	else if(id_dimension(selected_simplex)==1) 
	{
		display_grad_from_v(get_vertex(selected_simplex,0));
		display_grad_from_v(get_vertex(selected_simplex,1));
	}
}

@	@<display 1-2 gradient paths@>=
{
	if(id_dimension(selected_simplex)==2) 
	{
		display_grads_from_f(selected_simplex);
	}
	else if(id_dimension(selected_simplex)==1) 
	{
		display_grads_to_e(selected_simplex);
	}
}

@	@<display 2-3 gradient paths@>=
{
	if(id_dimension(selected_simplex)==3) 
	{
		display_grad_from_t(selected_simplex);
	}
	else if(id_dimension(selected_simplex)==2) 
	{
		if(!id_is_null(FindGrad23(coface(selected_simplex,0),40000)))
			display_grad_to_t(coface(selected_simplex,0),selected_simplex);
		if(!id_is_null(FindGrad23(coface(selected_simplex,1),40000)))
			display_grad_to_t(coface(selected_simplex,1),selected_simplex);
	}
}

@	@<display descending/ascending discs@>=
{
	if(id_dimension(selected_simplex)==2) display_descending_disc(selected_simplex);
	else if(id_dimension(selected_simplex)==1) display_ascending_disc(selected_simplex);
}

@	@<display link@>=
{
	if(id_dimension(selected_simplex)==0) display_link(selected_simplex);
	else if(id_dimension(selected_simplex)==1) display_link(get_vertex(selected_simplex,0));
	else  
	{
		simplex_id vid[4];
		
		if(id_dimension(selected_simplex)==2) get_triangle_vertices(selected_simplex,vid);
		else get_tetrahedron_vertices(selected_simplex,vid);
		display_link(vid[0]);
	}
}

@	@<display some info in the corner@>=
	
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);

	glLoadIdentity();
		
	glMatrixMode (GL_PROJECTION);
	glPushMatrix();  
	glLoadIdentity();
	glOrtho(0, winWidth, 0, winHeight,-10.0,10.0);
	
	glRasterPos2f(5.0, 5.0);
	glColor3f(1.0, 1.0, 1.0);
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);

	if(id_is_null(selected_simplex))
		sprintf(num_str, "(%f,%f,%f)",  headx,heady,headz);
	else
	{
		barycenter(selected_simplex,c);
		if(display_tree==NULL)
			sprintf(num_str,"(%f,%f,%f), value = %d",c[0],c[1],c[2],value(selected_simplex));
		else 
			sprintf(num_str,"(%f,%f,%f), count = %d",c[0],c[1],c[2],list_count(display_tree));
	}
	DrawStr(num_str);

	glPopMatrix(); 	
		
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);


@ @<display functions@>+=
void display_link(simplex_id v)
{
	struct face_struct *q;
	int i;
	GLfloat mat_emmission[4] = { 0.5, .1, .1, 0.0 }; 
	GLfloat mat_diffuse[]= {0.4,.4,.4,1.0};

	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	if(link_list==NULL)
		@<set up list of faces and edges in link@>@;
	
	@<display depth 0 triangles in link@>@;

	if(link_depth>0 && displ>0)
		@<display depth 1 triangles in link@>@;

	if(link_depth>1 && displ>0)
		@<display depth 2 triangles in link@>@;

	if(link_depth>2 && displ>0)
		@<display depth 3 triangles in link@>@;
		
	@<display edges in lower link@>@;
	
	if(displ>0)
		@<display edges not in lower link@>@;
		
	if(link_depth>1)
		@<display vertices in lower link@>@;	
}

@ The following assumes that the star of v in K is a union of tetrahedra.
The routine |vertex_star(v)| used below makes a list of tetrahedra containing |v|.
This list has a certain ordering, 
it starts with all tetrahedra for which |v| is the highest vertex,
then all tetrahedra for which |v| is the second highest vertex,
and so on.
@<set up list of faces and edges in link@>=
{
	list *l;
	hlist *edges_in_minus_link,*other_edges,*lone_vertices_in_minus_link;
	struct face_struct link;
	simplex_id t,f,vid[4],evid[2];
	int depth=0;
	
	// make |l| be a list of tetrahedra containing |v|
	// This list has a certain ordering,
	// It starts with all tetrahedra for which |v| is the highest vertex,
	// then all tetrahedra for which |v| is the second highest vertex,
	// and so on.
	l = vertex_star(v);
	
	list_initialize(&link_list,sizeof(struct face_struct));
	
	hlist_initialize(&edges_in_minus_link,sizeof(int),113,2,0);
	hlist_initialize(&other_edges,sizeof(int),113,2,0);
	hlist_initialize(&lone_vertices_in_minus_link,sizeof(int),113,1,0);
	
	@<put link faces on |link_list| and find edges also@>@;
	
	@<put indices of various depth faces on |link_depth_indices|@>@;
	
	@<put link edges on |link_list|@>@;
	
	@<put isolated vertices in minus link on |link_list|@>@;

	list_abandon(&l);
	hlist_abandon(&edges_in_minus_link);
	hlist_abandon(&other_edges);
	hlist_abandon(&lone_vertices_in_minus_link);
}

@ The list |l| starts with all tetrahedra in the link with depth 0,
i.e., for which |v| is the highest vertex.
(hence the opposite face is in the minus link of |v|).
Next comes those of depth 1, i.e., |v| is the next
highest vertex (so only the lowest edge is in the minus link of |v|).
Next comes depth 2, and finally depth 3.
@<put link faces on |link_list| and find edges also@>=
list_read_init(l);
while(!id_is_null(t=id_list_read(l)))
{
	if(!tetrahedron_is_in_K(t)) continue;
	get_tetrahedron_vertices(t,vid);
	@<see whether depth has just increased@>@;
	f = get_face(t,depth);
	get_triangle_vertices(f,link.vid);
	
	evid[0]=link.vid[0];
	evid[1]=link.vid[2];
	
	if(depth==0)
	{
		hlist_find_add(edges_in_minus_link,link.vid,&depth,NULL);
		hlist_find_add(edges_in_minus_link,evid,&depth,NULL);
	}
	else
	{
		hlist_find_add(other_edges,link.vid,&depth,NULL);
		hlist_find_add(other_edges,evid,&depth,NULL);
	}
	if(depth<2)
		hlist_find_add(edges_in_minus_link,link.vid+1,&depth,NULL);
	else
		hlist_find_add(other_edges,link.vid+1,&depth,NULL);
		
	if(depth==2)
		hlist_find_add(lone_vertices_in_minus_link,link.vid+2,&depth,NULL);
		
	get_oriented_face(&link,t,depth);

	list_push(link_list,&link);
}

@ @<see whether depth has just increased@>=
while(v!=vid[depth]) 
{
	link_depth_indices[depth] = list_count(link_list);
	depth++; 
	if(depth>=4) abort_message("internal setup_link error");
}


@ @<put indices of various depth faces on |link_depth_indices|@>=
link_depth = depth;
while(depth<4)
{
	link_depth_indices[depth] = list_count(link_list);
	depth++;
}

@ @<put link edges on |link_list|@>=

link.vid[2]=NULL_ID;
while(!hlist_is_empty(edges_in_minus_link))
{
	hlist_get(edges_in_minus_link,link.vid,&depth);
	list_push(link_list,&link);
}
link_depth_indices[4] = list_count(link_list);
while(!hlist_is_empty(other_edges))
{
	hlist_get(other_edges,link.vid,&depth);
	list_push(link_list,&link);
}
link_depth_indices[5] = list_count(link_list);

@ @<put isolated vertices in minus link on |link_list|@>=

link.vid[1]=NULL_ID;
while(!hlist_is_empty(lone_vertices_in_minus_link))
{
	hlist_get(lone_vertices_in_minus_link,link.vid,&depth);
	list_push(link_list,&link);
}


@	@<display depth 0 triangles in link@>=
	
glColor3f (1.0, 0.0, 0.0); 
glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);

glBegin(GL_TRIANGLES);
{
	for(i=0;i<link_depth_indices[0];i++)
		@<display one triangle in link@>@;
}
glEnd(); 

@	@<display depth 1 triangles in link@>=
{
	glColor3f (0.1, 0.2, 0.2);
	mat_emmission[0] = mat_emmission[1] = mat_emmission[2] = .1;
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	
	glBegin(GL_TRIANGLES);
	{
		for(;i<link_depth_indices[1];i++)
			@<display one triangle in link@>@;
	}
	glEnd(); 
}

@	@<display depth 2 triangles in link@>=
{
	glColor3f (0.1, 0.1, 0.2);
	mat_emmission[0] = mat_emmission[1] = mat_emmission[2] = .1;
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glBegin(GL_TRIANGLES);
	{
		for(;i<link_depth_indices[2];i++)
			@<display one triangle in link@>@;
	}
	glEnd(); 
}

@	@<display depth 3 triangles in link@>=
{
	glColor3f (0.1, 0.1, 0.1);
	mat_emmission[0] = mat_emmission[1] = mat_emmission[2] = .1;
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glBegin(GL_TRIANGLES);
	{
		for(;i<link_depth_indices[3];i++)
			@<display one triangle in link@>@;
	}
	glEnd(); 
}


@ @<display one triangle in link@>=
{
	q = (struct face_struct *)list_entry(link_list,i);
	@<display the triangle |q|@>@;
}

@ @<display the triangle |q|@>=
{
	glNormal3fv(q->normal);
	glVertex3fv(id2vertex(q->vid[0])->coords);    
	glVertex3fv(id2vertex(q->vid[1])->coords);    
	glVertex3fv(id2vertex(q->vid[2])->coords);    
}

@ @<display edges in lower link@>=
	glColor3f (1.0, 0.0, 0.0);  
	mat_emmission[0] = 1.0;
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
	glLineWidth(3.0);
	glBegin(GL_LINES);
	{
		for(i=link_depth_indices[3];i < link_depth_indices[4];i++)
		{
			q = (struct face_struct *)list_entry(link_list,i);
			glVertex3fv(id2vertex(q->vid[0])->coords);    
			glVertex3fv(id2vertex(q->vid[1])->coords);    
		}
	}
	glEnd(); 
	glLineWidth(1.0);

@ @<display edges not in lower link@>=
{
	glColor3f (0.2, 0.2, 0.2);
	mat_emmission[0] = mat_emmission[1] = mat_emmission[2] = .1;
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
	
	glLineWidth(2.0);
	glBegin(GL_LINES);
	{
		for(i=link_depth_indices[4];i < link_depth_indices[5];i++)
		{
			q = (struct face_struct *)list_entry(link_list,i);
			glVertex3fv(id2vertex(q->vid[0])->coords);    
			glVertex3fv(id2vertex(q->vid[1])->coords);    
		}
	}
	glEnd(); 
	glLineWidth(1.0);
}

@ @<display vertices in lower link@>=
{
	glPointSize(3.0);
	glBegin(GL_POINTS);
	{
		for(i=link_depth_indices[5];i < list_count(link_list);i++)
		{
			q = (struct face_struct *)list_entry(link_list,i);
			glVertex3fv(id2vertex(q->vid[0])->coords);    
		}
	}
	glEnd(); 
	glPointSize(1.0);
}


@ @<display functions@>+=
void display_boundary(void)
{
	uint32_t i;
	GLfloat mat_diffuse[] = { 0.9, .9, .3, 0.5 }; 
	struct face_struct *q;
	GLfloat mat_emmission[] = { 0.0, .0, .0, 0.0 }; 

	if(boundary_list==NULL) 
		@<set up list of boundary faces@>@;
	glColor3f (1.0, 0.0, 0.0);  
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);


	
	glBegin(GL_TRIANGLES);
	{
		for(i=0;i<list_count(boundary_list);i++)
		{
			q= (struct face_struct*)list_entry(boundary_list,i);
			@<display the triangle |q|@>@;
		}
	}
	glEnd(); 
}

@ @<set up list of boundary faces@>=
{
	int j;
	simplex_id t,s,temp;
	struct face_struct face;
	
	list_initialize(&boundary_list,sizeof(struct face_struct));
	
	for(i=0;i< number_of_tetrahedra;i++)
	{
		t = tetrahedron_id(i);
		
		for(j=0;j<4;j++)
		{
			s = id2tetrahedron(t)->face_ids[j];
			if(!tetrahedron_is_in_K(id2tetra_id(s)))
			{
				if(tetrahedron_is_in_K(t))
				{
					get_oriented_face(&face,t,j);
					list_push(boundary_list,&face);
				}
				else if(triangle_is_in_K(s) && s>t )
				{
					get_oriented_face(&face,t,j);

					list_push(boundary_list,&face);
					// display face with both orientations
					// since both faces are on the boundary.
					negatev(face.normal);
					temp = face.vid[2];
					face.vid[2] = face.vid[1];
					face.vid[1] =temp;
					
					list_push(boundary_list,&face);
				}
			}
		}
	}
}

@ The following routine takes the |k|-th face of the tetrahedron |t|
and fills in its vertices and unit normal in the structure |face|.
It makes sure the order of the vertices is such that the side of this face
towards the |k|-th vertex of |t| is the back face.
@<utility functions@>+=
void get_oriented_face(struct face_struct *face,simplex_id t,int k)
{
	GLfloat d;
	simplex_id f,temp;
	
	f = get_face(t,k);
	get_triangle_vertices(f,face->vid);
	
	affine_crossv(id2vertex(face->vid[0])->coords,id2vertex(face->vid[1])->coords,
					id2vertex(face->vid[2])->coords,face->normal);
	
	makeunitv(face->normal);
	d = dotv(face->normal,id2vertex(id2tetrahedron(t)->vertex_ids[k])->coords) 
			- dotv(face->normal,id2vertex(face->vid[0])->coords);

	if(d>0) 
	{
		// exchange two vertices to make orientation correct
		temp = face->vid[2];
		face->vid[2] = face->vid[1];
		face->vid[1] =temp;
		negatev(face->normal);
	}

}

@ @<display functions@>+=
void display_grad_to_t(simplex_id tau,simplex_id f)
{
	simplex_id t;
	GLfloat *p,*q,*r,*s,c[3];
	simplex_id vl[4];
	GLfloat mat_emmission[] = { 0.5, .5, .5, 1.0 }; 
	GLfloat mat_diffuse[]= {0.5,.5,.5,1.0};

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	glBegin(GL_LINE_STRIP); 
	{
		t=tau;
		get_triangle_vertices(f,vl);
		p = id2vertex(vl[0])->coords;     
		q = id2vertex(vl[1])->coords;     
		r = id2vertex(vl[2])->coords;  
		c[0] =  (p[0]+q[0]+r[0])/3;
		c[1] =  (p[1]+q[1]+r[1])/3;
		c[2] =  (p[2]+q[2]+r[2])/3;
		glVertex3fv(c);
		
		while( tetrahedron_is_in_K(t) && !tetrahedron_is_critical(t))
		{
			get_tetrahedron_vertices(t,vl);
			p = id2vertex(vl[0])->coords;     
			q = id2vertex(vl[1])->coords;     
			r = id2vertex(vl[2])->coords;  
			s = id2vertex(vl[3])->coords;  
			c[0] =  (p[0]+q[0]+r[0]+s[0])/4;
			c[1] =  (p[1]+q[1]+r[1]+s[1])/4;
			c[2] =  (p[2]+q[2]+r[2]+s[2])/4;
			glVertex3fv(c);
			f = r32(t);
			if (triangle_is_deadend(&f)) break;
			t = other_coface(f,t);
			if (tetrahedron_is_deadend(t)) break;
			get_triangle_vertices(f,vl);
			p = id2vertex(vl[0])->coords;     
			q = id2vertex(vl[1])->coords;     
			r = id2vertex(vl[2])->coords;  
			c[0] =  (p[0]+q[0]+r[0])/3;
			c[1] =  (p[1]+q[1]+r[1])/3;
			c[2] =  (p[2]+q[2]+r[2])/3;
			glVertex3fv(c);
		}
	}
	glEnd(); 
}

@ @<display functions@>+=
void display_grad_from_t(simplex_id t)
{
	simplex_id f;
	
	list_read_init(crit[2]);
	while (!id_is_null(f=id_list_read(crit[2])))  // run through all critical triangles
	{
		if(t==FindGrad23(coface(f,0),40000)) display_grad_to_t(coface(f,0),f);
		if(t==FindGrad23(coface(f,1),40000)) display_grad_to_t(coface(f,1),f);
	}
}

@ @<display functions@>+=
void display_grad_from_v(simplex_id u)
{
	simplex_id v;
	simplex_id e;
	GLfloat mat_emmission[] = { 0.5, .5, .5, 1.0 }; 
	GLfloat mat_diffuse[]= {0.5,.5,.5,1.0};

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	glBegin(GL_LINE_STRIP); 
	{
		v=u;
		glVertex3fv(id2vertex(v)->coords);
		while(!vertex_is_critical(v))
		{
			e = r01(v);
			v = other_vertex_in_edge(v,e);
			glVertex3fv(id2vertex(v)->coords);
		}
	}
	glEnd(); 
}

@ @<display functions@>+=
void display_grad_to_v(simplex_id v)
{
	simplex_id e;
	
	list_read_init(crit[1]);
	while (!id_is_null(e=id_list_read(crit[1])))  // run through all critical edges
	{
		if(v==FindGrad01(get_vertex(e,0),-40000)) display_grad_from_v(get_vertex(e,0));
		if(v==FindGrad01(get_vertex(e,1),-40000)) display_grad_from_v(get_vertex(e,1));
	}
}

@ @<display functions@>+=
void display_grads_to_e(simplex_id e)
{
	struct tree12_struct *q;
	int32_t i;
	GLfloat mat_emmission[] = { 0.5, .5, .5, 1.0 }; 
	GLfloat mat_diffuse[]= {0.5,.5,.5,1.0};

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	if(tree12==NULL)
		@<set up list of faces which eventually flow to |e|@>@;
	
	glBegin(GL_LINES);
	{
		for(i=0;i<list_count(tree12);i++)
		{
			q = (struct tree12_struct *)list_entry(tree12,i);
			glVertex3fv(q->p);    
			glVertex3fv(q->r);    
		}
	}
	glEnd(); 
}

@ @<set up list of faces which eventually flow to |e|@>=
{
	hlist *triangles;
	int32_t start;
	struct grad12_struct rp;
	simplex_id ep;
	simplex_id f;
	int k,j;
	struct tree12_struct r;
	
	hlist_initialize(&triangles,sizeof(struct grad12_struct),103,1,0);
	list_initialize(&tree12,sizeof(struct tree12_struct));
	
	start = find_all_backward_grad12_paths(e,triangles,NULL,2);
	
	while(hlist_get(triangles,&f,&rp))
	{
		if((rp.flags&4)==0) continue;
		k = rp.flags&3;
		if(k == 3) continue;
		ep = get_edge(f,k);
		edge_barycenter(ep,r.r);
		
		for(j=0;j<3;j++)
		{
			if(k==j || rp.links[j] < -1) continue;
			ep = get_edge(f,j);
			edge_barycenter(ep,r.p);
			list_push(tree12,&r);
		}
	}
	hlist_abandon(&triangles);
}

@ @<display functions@>+=
void display_grads_from_f(simplex_id f)
{
	struct tree12_struct *q;
	int32_t i;
	GLfloat mat_emmission[] = { 0.5, .5, .5, 1.0 }; 
	GLfloat mat_diffuse[]= {0.5,.5,.5,1.0};

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	if(tree12==NULL)
		@<set up list of faces which flow from |f|@>@;
	
	glBegin(GL_LINES);
	{
		for(i=0;i<list_count(tree12);i++)
		{
			q = (struct tree12_struct *)list_entry(tree12,i);
			glVertex3fv(q->p);    
			glVertex3fv(q->r);    
		}
	}
	glEnd(); 
}

@ @<set up list of faces which flow from |f|@>=
{
	hlist *edges;
	list *todo;
	int32_t crit_id;
	struct grad12_struct *p;
	
	hlist_initialize(&edges,sizeof(struct grad12_struct),113,2,0);
	list_initialize(&todo,sizeof(int32_t));
	list_initialize(&tree12,sizeof(struct tree12_struct));
	
	
	if(disp_option) crit_id = find_all_grad12_paths(f,edges,2);
	else  crit_id = find_all_grad12_paths(f,edges,0);
	
	while(crit_id>=0)
	{
		@<add paths from the |crit_id| node of |edges| or something@>@;
		p = (struct grad12_struct *)hlist_entry(edges,crit_id);
		crit_id = p->links[1];
	}
	hlist_abandon(&edges);
	list_abandon(&todo);
}

@ I should figure out what this does, but I'm too lazy right now.
@<add paths from the |crit_id| node of |edges| or something@>=
{
	int32_t next;
	int32_t id;
	simplex_id e,ep;
	simplex_id vl[2];
	struct tree12_struct r;
	struct grad12_struct *pp;
	
	list_push(todo,&crit_id);
	
	while(!list_is_empty(todo))
	{
		list_pop(todo,&next);
		p = (struct grad12_struct *)hlist_entry(edges,next);
		
		i = p->flags&3;
		if(i==3) id = p->links[0];
		else id = p->links[i];
		
		e = p->e;
		get_edge_vertices(e,vl);
		edge_barycenter(e,r.p);
		
		while(id>=0)
		{
			pp = (struct grad12_struct *)hlist_entry(edges,id);
			i = pp->flags&3;
			ep = pp->e;
			edge_barycenter(ep,r.r);
			list_push(tree12,&r);
			if((pp->flags&64)==0)
			{
				list_push(todo,&id);
				pp->flags|=64;
			}
			i = edge_verts_in_triangle(vl,r12(&ep));
			if(i<0) abort_message("error");
			id = pp->links[i];
		}
	}
}

@ @<utility functions@>+=
void barycenter(simplex_id t,GLfloat c[3])
{
	simplex_id vl[4];
	GLfloat *p,*q,*r,*s;
	
	switch(id_dimension(t))
	{
		case 0:
			p = id2vertex(t)->coords;     
			c[0] =  p[0];
			c[1] =  p[1];
			c[2] =  p[2];
			break;
		case 1:
			get_edge_vertices(t,vl); 
			p = id2vertex(vl[0])->coords;     
			q = id2vertex(vl[1])->coords;     
			c[0] =  (p[0]+q[0])/2;
			c[1] =  (p[1]+q[1])/2;
			c[2] =  (p[2]+q[2])/2;
			break;
		case 2:
			get_triangle_vertices(t,vl); 
			p = id2vertex(vl[0])->coords;     
			q = id2vertex(vl[1])->coords;     
			r = id2vertex(vl[2])->coords;  
			c[0] =  (p[0]+q[0]+r[0])/3;
			c[1] =  (p[1]+q[1]+r[1])/3;
			c[2] =  (p[2]+q[2]+r[2])/3;
			break;
		case 3:
			get_tetrahedron_vertices(t,vl); 
			p = id2vertex(vl[0])->coords;     
			q = id2vertex(vl[1])->coords;     
			r = id2vertex(vl[2])->coords;  
			s = id2vertex(vl[3])->coords;  
			c[0] =  (p[0]+q[0]+r[0]+s[0])/4;
			c[1] =  (p[1]+q[1]+r[1]+s[1])/4;
			c[2] =  (p[2]+q[2]+r[2]+s[2])/4;
			break;
	}
}


@ @<utility functions@>+=
void edge_barycenter(simplex_id e,GLfloat x[3])
{
	simplex_id vl[2];
	GLfloat *p,*q;
	
	get_edge_vertices(e,vl);
	p = id2vertex(vl[0])->coords;     
	q = id2vertex(vl[1])->coords;     
	x[0] = (p[0]+q[0])/2;
	x[1] = (p[1]+q[1])/2;
	x[2] = (p[2]+q[2])/2;
}



@ @<display functions@>+=
void display_ascending_disc(simplex_id e)
{
	if(display_tree==NULL)
		@<set up list of faces in the ascending disc@>@;
	
	@<display all the triangles in |display_tree|@>@;
}

@ @<set up list of faces in the ascending disc@>=
{
	hlist *triangles;
	struct grad12_struct r;
	struct tri_tree tt;
	simplex_id f;
	
	hlist_initialize(&triangles,sizeof(struct grad12_struct),113,1,0);

	if(disp_option) find_all_backward_grad12_paths(e,triangles,NULL,0+2);
	 find_all_backward_grad12_paths(e,triangles,NULL,0);

	list_initialize(&display_tree,sizeof(struct tri_tree));
	while (hlist_get(triangles,&f,&r))
	{
		if(triangle_is_xdeadend(&f)) continue;
		
		setup_tri_tree_entry(&tt,f,r.count);
		
		list_push(display_tree,&tt);
	}
	hlist_abandon(&triangles);
}

@ @<display all the triangles in |display_tree|@>=
{
	int32_t i;
	struct tri_tree *qq;
	struct face_struct *q;
	GLfloat mat_diffuse[] = { 0.5, .5, .5, 1.0 }; 
	GLfloat mat_emmission[] = { 0.0, .0, .0, 0.0 }; 

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, mat_emmission);

	glBegin(GL_TRIANGLES);
	{
		for(i=0;i<list_count(display_tree);i++)
		{
			qq = (struct tri_tree *)list_entry(display_tree,i);
			q = &qq->face;
			mat_diffuse[0] =  .05*(qq->count);
			glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, mat_diffuse);
			mat_diffuse[0] =  0.0;
			glMaterialfv(GL_BACK, GL_AMBIENT_AND_DIFFUSE, mat_diffuse);
			if(qq->count != 0 || disp_option!=0) 
				@<display the triangle |q|@>@;
		}
	}
	glEnd(); 
}



@ @<display functions@>+=
void display_descending_disc(simplex_id f)
{
	if(display_tree==NULL)
		@<set up list of faces in the descending disc@>@;
		
	@<display all the triangles in |display_tree|@>@;
}

@ @<set up list of faces in the descending disc@>=
{
	hlist *edges;
	struct grad12_struct r;
	struct tri_tree tt;
	simplex_id vl[2];
	
	hlist_initialize(&edges,sizeof(struct grad12_struct),113,2,0);

	if(disp_option) find_all_grad12_paths(f,edges,4+2);
	else find_all_grad12_paths(f,edges,4+0);

	list_initialize(&display_tree,sizeof(struct tri_tree));
	
	setup_tri_tree_entry(&tt,f,1);
	list_push(display_tree,&tt);
	
	while (hlist_get(edges,vl,&r))
	{
		if(edge_is_critical(&(r.e))) continue;
		if(edge_is_deadend(&(r.e))) continue;
		if(r.count==0) continue;
		setup_tri_tree_entry(&tt,r12(&(r.e)),r.count);
		list_push(display_tree,&tt);
	}
	hlist_abandon(&edges);
}

@ @<utility functions@>+=
void setup_tri_tree_entry(struct tri_tree *t,simplex_id f,int count)
{
	simplex_id temp;

	t->id = f;
	get_triangle_vertices(f,t->face.vid);

	affine_crossv(id2vertex(t->face.vid[0])->coords,
				id2vertex(t->face.vid[1])->coords,
				id2vertex(t->face.vid[2])->coords,t->face.normal);

	makeunitv(t->face.normal);
	if(count<0)
	{
		t->count = -count;
		negatev(t->face.normal);
		// Reverse some vertices to change orientation
		temp = t->face.vid[2];
		t->face.vid[2] = t->face.vid[1];
		t->face.vid[1] = temp;
	}
	else t->count = count;
}

@ @<display functions@>+=
void
display_crit0(GLenum mode)
{
	GLfloat *p;
	simplex_id v;
	GLfloat mat_emmission[] = { 0.05, .0, .1, 0.0 }; 
	GLfloat mat_emmission_sel[] = { 0.5, .5, 1.0, 0.0 }; 
	GLfloat mat_diffuse[]= {0.4,.1,.8,1.0};

	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);

	glColor3f (0.5, 0.0, 1.0);  
	list_read_init(crit[0]);
	while (!id_is_null(v=id_list_read(crit[0])))  // run through all critical vertices
	{
		if(v == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission_sel);
		}
		glPushMatrix(); 
		p = id2vertex(v)->coords;     
		glTranslatef(p[0],p[1],p[2]);  
		if (mode == GL_SELECT)  glLoadName (v); 
		glutSolidSphere(scalecrit0, 8, 8); 
		glPopMatrix(); 	
		if(v == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
		}
	}
}

@ @<display functions@>+=
void
display_crit1(GLenum mode)
{
	GLfloat mat_emmission[] = { 0.0, .1, .05, 0.0 }; 
	GLfloat mat_emmission_sel[] = { 0.5, 1.0, .5, 0.0 }; 
	GLfloat mat_diffuse[]= {0.1,.8,.4,1.0};
	GLfloat *p,*q;
	simplex_id e;

	glColor3f (0.0, 1.0, .5); 
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	list_read_init(crit[1]);
	while (!id_is_null(e=id_list_read(crit[1])))  // run through all critical edges
	{
		if(e == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission_sel);
		}
		glPushMatrix();  
		p = id2vertex(get_vertex(e,0))->coords;     
		q = id2vertex(get_vertex(e,1))->coords;     
		glTranslatef((p[0]+q[0])/2,(p[1]+q[1])/2,(p[2]+q[2])/2);   
		if (mode == GL_SELECT)  glLoadName (e); 
		glutSolidSphere(scalecrit1, 8, 8); 
		glPopMatrix(); 	
		glBegin(GL_LINES); 
		{
			glVertex3fv(p); 
			glVertex3fv(q); 
		}
		glEnd(); 
		if(e == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
		}
	}
}

@ @<display functions@>+=
void
display_crit2(GLenum mode)
{
	GLfloat mat_emmission[] = { 0.1, .05, .0, 0.0 }; 
	GLfloat mat_emmission_sel[] = { 1.0, .5, .5, 0.0 }; 
	GLfloat mat_diffuse[]= {0.8,.4,.1,1.0};
	GLfloat *p,*q,*r,c[3];
	simplex_id f;
	simplex_id vl[3];

	glColor3f (1.0, .5, .0); 
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	list_read_init(crit[2]);
	while (!id_is_null(f=id_list_read(crit[2])))  // run through all critical triangles
	{
		if(f == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission_sel);
		}
		glPushMatrix(); 
		get_triangle_vertices(f,vl); 
		p = id2vertex(vl[0])->coords;     
		q = id2vertex(vl[1])->coords;     
		r = id2vertex(vl[2])->coords;  
		c[0] =  (p[0]+q[0]+r[0])/3;
		c[1] =  (p[1]+q[1]+r[1])/3;
		c[2] =  (p[2]+q[2]+r[2])/3;
		 
		glTranslatef(c[0],c[1],c[2]);  
		if (mode == GL_SELECT)  glLoadName (f); 
		glutSolidSphere(scalecrit2, 6, 6); 
		glPopMatrix(); 	
		glBegin(GL_LINES); 
		{
			glVertex3fv(c); 
			glVertex3f((p[0]+q[0])/2,(p[1]+q[1])/2,(p[2]+q[2])/2); 
			glVertex3fv(c); 
			glVertex3f((p[0]+r[0])/2,(p[1]+r[1])/2,(p[2]+r[2])/2); 
			glVertex3fv(c); 
			glVertex3f((r[0]+q[0])/2,(r[1]+q[1])/2,(r[2]+q[2])/2); 
		}
		glEnd(); 
		if(f == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
		}
	}
}

@ @<display functions@>+=
void
display_crit3(GLenum mode)
{
	GLfloat mat_emmission[] = { 0.05, .0, .1, 0.0 }; 
	GLfloat mat_emmission_sel[] = { 0.5, .5, 1.0, 0.0 }; 
	GLfloat mat_diffuse[]= {0.4,.1,.8,1.0};
	GLfloat *p,*q,*r,*s,c[3];
	simplex_id t;
	simplex_id vl[4];

	glColor3f (1.0, .3, .2); 
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
	glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,mat_diffuse);
	
	list_read_init(crit[3]);
	while (!id_is_null(t=id_list_read(crit[3])))  // run through all critical tetrahedra
	{
		if(t == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission_sel);
		}
		glPushMatrix(); 
		get_tetrahedron_vertices(t,vl); 
		p = id2vertex(vl[0])->coords;     
		q = id2vertex(vl[1])->coords;     
		r = id2vertex(vl[2])->coords;  
		s = id2vertex(vl[3])->coords;  
		c[0] =  (p[0]+q[0]+r[0]+s[0])/4;
		c[1] =  (p[1]+q[1]+r[1]+s[1])/4;
		c[2] =  (p[2]+q[2]+r[2]+s[2])/4;
		 
		glTranslatef(c[0],c[1],c[2]);   
		if (mode == GL_SELECT)  glLoadName (t); 
		glutSolidSphere(scalecrit3, 4, 4); 
		glPopMatrix(); 	
		glBegin(GL_LINES); 
		{
			glVertex3fv(c); 
			glVertex3f((p[0]+q[0]+r[0])/3,(p[1]+q[1]+r[1])/3,(p[2]+q[2]+r[2])/3); 
			glVertex3fv(c); 
			glVertex3f((s[0]+q[0]+r[0])/3,(s[1]+q[1]+r[1])/3,(s[2]+q[2]+r[2])/3); 
			glVertex3fv(c); 
			glVertex3f((p[0]+s[0]+r[0])/3,(p[1]+s[1]+r[1])/3,(p[2]+s[2]+r[2])/3); 
			glVertex3fv(c); 
			glVertex3f((p[0]+q[0]+s[0])/3,(p[1]+q[1]+s[1])/3,(p[2]+q[2]+s[2])/3); 
		}
		glEnd(); 
		if(t == selected_simplex)
		{
			glMaterialfv(GL_FRONT, GL_EMISSION, mat_emmission);
		}
	}
}

@ @<utility functions@>+=
void DrawStr(const char *str)
{
	int i = 0;
	
	if(!str) return;
        
	while(str[i])
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, str[i]);
		i++;
	}
}




@ This is called when a mouse button is pressed or released.
@<callback functions@>+=
void
mouse(int button, int state, int x, int y)
{
	/* Rotate the scene with the left mouse button. */
	if (button == GLUT_LEFT_BUTTON) 
	{
		if (state == GLUT_DOWN) 
		{
		  	rotating = 1;
		  	startx = x;
		  	starty = y;
		}
		if (state == GLUT_UP) 
		{
		  	rotating = 0;
		}
	}
	else if (button == GLUT_MIDDLE_BUTTON)
	{
		if (state == GLUT_DOWN) 
			@<select the critical simplex pointed to by the mouse@>@; 
	}
}

@ @<select the critical simplex pointed to by the mouse@>=
{
	GLuint selectBuf[1024],*p;
	GLint hits;
	GLint viewport[4]; 
	int i;
	int dim;
	
	glGetIntegerv (GL_VIEWPORT, viewport);
	glSelectBuffer (1024, selectBuf);
	glRenderMode (GL_SELECT);
	glInitNames();
	glMatrixMode (GL_PROJECTION);
	glPushMatrix ();
	glLoadIdentity(); 
	gluPickMatrix ((GLdouble) x, (GLdouble) (viewport[3] - y), 5.0, 5.0, viewport); 
    gluPerspective(40.0, (float)winWidth/(float)winHeight, .01*scale,5.0*scale);
	@<set up model view@>@; 

	glPushName(0); 
	glPushName(0); 
	display_crit0(GL_SELECT);
	glPopName(); 
	glLoadName(1); 
	glPushName(0); 
	display_crit1(GL_SELECT);
	glPopName(); 
	glLoadName(2); 
	glPushName(0); 
	display_crit2(GL_SELECT);
	glPopName(); 
	glLoadName(3); 
	glPushName(0); 
	display_crit3(GL_SELECT);
	
	glMatrixMode (GL_PROJECTION);
	glPopMatrix ();
	glMatrixMode(GL_MODELVIEW); 
	glFlush ();
	
	@<find closest critical simplex near mouse@>@;
	abandon_lists();
	@<indicate display should change@>@;
}

@ @<find closest critical simplex near mouse@>=
{
	GLuint bestz;
	
	hits = glRenderMode (GL_RENDER); 
	if(hits<0) abort_message("hit buffer overflow");
	else 
	{
		p = selectBuf;
		selected_simplex = NULL_ID;
		for (i = 0; i < hits; i++)
		{
			if((*p++)!=2) abort_message("bad hit buffer");
			if(i==0 || bestz>*p)
			{
				bestz = *p++;
				p++;
				dim = *p++;
				selected_simplex = *p++;
			}
			else p+=4;
		}
	}
}



@ This is called when the mouse is dragged, indicating the user wants to
move the view in some way.
The algorithm is as follows.
Let {\bf v} be the vector from the middle of the window to the last mouse position.
Let {\bf w} be the vector from the middle of the window 
to the current mouse position.
Let |lv| and |lw| be the lenngths of {\bf v} and {\bf w}.
Let $\theta$ be the angle between {\bf v} and {\bf w}.
We then rotate the scene by the angle $\theta$, as long as
$\theta$ is not too large and
{\bf v} and {\bf w} are not too small,
otherwise we rotate by somewhat less than $\theta$.
(This avoids rapid rotation if the user moves the mouse near the center.)

The direction the user looks at also changes.
If $lv < lw$, i.e., the user drags the mouse toward the center
of the window, then the viewpoint moves in the direction
$-{\bf v}-{\bf w}$ by an amount proportional to $lw - lv$.
Otherwise  the viewpoint moves in the direction
${\bf v}+{\bf w}$.
These movements are buried in the code below.

I should really change the code so that a point under the mouse stays under
the mouse.  To really do this you would need to go through the selection stuff,
which i suspect would be too slow.
But you could ask that a point at middle distance under the mouse not move.
@<callback functions@>+=
void
motion(int x, int y)
{
	double mx;
	double my;
	double vx,vy,wx,wy,lv,lw,area,t,c,s,a,b;

	if (rotating) 
	{
		mx = winWidth/2;
		my = winHeight/2;
		vx = startx-mx;
		vy = starty-my;
		wx = x-mx;
		wy = y-my;
		lv = sqrt(vx*vx+vy*vy);
		lw = sqrt(wx*wx+wy*wy);
		area = (vx*wy-vy*wx);
		
		@<rotate |up| and |side|@>@;
		
		t = (lv-lw)/(lv+lw+1);
		b = t*(vx+wx)/mx;
		a = -t*(vy+wy)/my;
		headx -= a*upx+b*sidex;
		heady -= a*upy+b*sidey;
		headz -= a*upz+b*sidez;
		changehead();
		startx = x;
		starty = y;
		@<indicate display should change@>@;
	}
}

@ This rotates the scene in the window by an angle $\arcsin(s)$
about the center of the window.
@<rotate |up| and |side|@>=
{
	a = mx*my/50.0;
	b = lv*lw;
	
	if(b>a)
		s = area/b;
	else s = area/a;

	if(s>.4) s=.4;
	else if(s<-.4) s = -.4;

	c = sqrt(1-s*s);
	
	a = sidex*c + upx*s;
	upx = upx*c - sidex*s;
	sidex = a;
	
	a = sidey*c + upy*s;
	upy = upy*c - sidey*s;
	sidey = a;
	
	a = sidez*c + upz*s;
	upz = upz*c - sidez*s;
	sidez = a;
}

@ @<callback functions@>+=
void
keyboard(unsigned char c, int x, int y)
{
	double a,b,d;
	GLfloat sel_coords[3];
	
	switch (c) {
	case 'Q':  
	case 'q':
	case 27:  /* Escape quits. */
		exit(0);
		break;
	case 'v':
	case 'V':
		posx+=headx*scalemove;
		posy+=heady*scalemove;
		posz+=headz*scalemove;
		break;
	case ' ':
		posx-=headx*scalemove;
		posy-=heady*scalemove;
		posz-=headz*scalemove;
		break;
	case 'b':
	case 'B':
		b = .01;
		a = b*scalemove1;
		@<rotate to the side@>@;
		break;
	case 'c':
	case 'C':
		b = -.01;
		a = b*scalemove1;
		@<rotate to the side@>@;
		break;
	case 'g':
	case 'G':
		if(!id_is_null(selected_simplex))
		{
			barycenter(selected_simplex,sel_coords);
			d = distance(posx,posy,posz,sel_coords[0],sel_coords[1],sel_coords[2])+scale_epsilon;
			@<point head more towards selected simplex@>@;
			b = .01;
			a = b*d;
			@<rotate to the side@>@;
		}
		break;
	case 'f':
	case 'F':
		if(!id_is_null(selected_simplex))
		{
			barycenter(selected_simplex,sel_coords);
			d = distance(posx,posy,posz,sel_coords[0],sel_coords[1],sel_coords[2])+scale_epsilon;
			@<point head more towards selected simplex@>@;
			b = -.01;
			a = b*d;
			@<rotate to the side@>@;
		}
		break;
	}
	@<indicate display should change@>@;
}

@ @<rotate to the side@>=
	headx += b*sidex;
	heady += b*sidey;
	headz += b*sidez;
	posx += a*sidex;
	posy += a*sidey;
	posz += a*sidez;
	changehead();
	
@ @<point head more towards selected simplex@>=
	headx = .95*headx - .05*(sel_coords[0]-posx)/d;
	heady = .95*heady - .05*(sel_coords[1]-posy)/d;
	headz= .95*headz - .05*(sel_coords[2]-posz)/d;

@ @<callback functions@>+=
void reshape(int width, int height)
{
    glViewport(0, 0, width, height);
    winWidth = width;
    winHeight = height;
	glMatrixMode(GL_PROJECTION);     
	glLoadIdentity(); 
    gluPerspective(40.0, (float)width/(float)height, .01*scale,5.0*scale);
	@<indicate display should change@>@;
}

@ @<set up model view@>=
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(model_view_matrix);
	glTranslatef(-posx, -posy, -posz); 

@ @<utility functions@>+=
void abandon_lists(void)
{
	if(display_tree!=NULL) list_abandon(&display_tree);
	if(tree12!=NULL) list_abandon(&tree12);
	if(link_list!=NULL) list_abandon(&link_list);
}

@ @<basic vector operations@>=
void
makeunit(GLfloat *x,GLfloat *y,GLfloat *z)
{
	double r;
	
	r = sqrt((*x)*(*x)+(*y)*(*y)+(*z)*(*z));
	(*x)/=r;
	(*y)/=r;
	(*z)/=r;
}

void
makeunitv(GLfloat *w)
{
	double r;
	
	r = sqrt((w[0])*(w[0])+(w[1])*(w[1])+(w[2])*(w[2]));
	w[0]/=r;
	w[1]/=r;
	w[2]/=r;
}

@ @<basic vector operations@>=
double
dot(GLfloat x,GLfloat y,GLfloat z,GLfloat a,GLfloat b,GLfloat c)
{
	return x*a+y*b+z*c;
}

double
dotv(GLfloat *w,GLfloat *c)
{
	return w[0]*c[0]+w[1]*c[1]+w[2]*c[2];
}

@ @<basic vector operations@>=
void
negatev(GLfloat *w)
{
	w[0]=-w[0];
	w[1]=-w[1];
	w[2]=-w[2];
}

@ @<basic vector operations@>=
double
distance(GLfloat wx,GLfloat wy,GLfloat wz,GLfloat cx,GLfloat cy,GLfloat cz)
{
	return sqrt((wx-cx)*(wx-cx)+(wy-cy)*(wy-cy)+(wz-cz)*(wz-cz));
}

double
distancev(GLfloat *w,GLfloat *c)
{
	return sqrt((w[0]-c[0])*(w[0]-c[0])+(w[1]-c[1])*(w[1]-c[1])+(w[2]-c[2])*(w[2]-c[2]));
}

@ @<basic vector operations@>=
void
cross(GLfloat x,GLfloat y,GLfloat z,GLfloat a,GLfloat b,GLfloat c,
			GLfloat *ax,GLfloat *ay,GLfloat *az)
{
	*ax = y*c-z*b;
	*ay = z*a-x*c;
	*az = x*b-y*a;
}

void
affine_crossv(GLfloat *p0,GLfloat *p1,GLfloat *p2,GLfloat *normal)
{
	normal[0] = (p1[1]-p0[1])*(p2[2]-p0[2])-(p1[2]-p0[2])*(p2[1]-p0[1]);
	normal[1] = (p1[2]-p0[2])*(p2[0]-p0[0])-(p1[0]-p0[0])*(p2[2]-p0[2]);
	normal[2] = (p1[0]-p0[0])*(p2[1]-p0[1])-(p1[1]-p0[1])*(p2[0]-p0[0]);
}


@ After modifying head, call this to make sure that
    head, up, and side are orthonormal
@<utility functions@>+=
void
changehead(void)
{
	double a;
	makeunit(&headx,&heady,&headz);
	a = dot(headx,heady,headz,upx,upy,upz);
	upx -= a*headx;
	upy -= a*heady;
	upz -= a*headz;
	if(fabs(upx)<.01&&fabs(upy)<.01&&fabs(upz)<.01)
	{
		if(fabs(headx)>.2)
		{
			upx = heady;
			upy = -headx;
			upz = 0;
		}
		else
		{
			upx = 0;
			upy = -headz;
			upz = heady;
		}
	}
	makeunit(&upx,&upy,&upz);
	cross(upx,upy,upz,headx,heady,headz,&sidex,&sidey,&sidez);
}



@ 
@d sidex model_view_matrix[0]
@d sidey model_view_matrix[4]
@d sidez model_view_matrix[8]
@d upx   model_view_matrix[1]
@d upy   model_view_matrix[5]
@d upz   model_view_matrix[9]
@d headx model_view_matrix[2]
@d heady model_view_matrix[6]
@d headz model_view_matrix[10]

@<globals, etc.@>=
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glut.h>
#include "main.h"
#include "simplex.h"
#include "homology.h"
#include "cancel.h"

extern int global_persist;

struct tree12_struct
{
	GLfloat p[3];
	GLfloat r[3];
};

struct face_struct
{
	simplex_id vid[3];
	GLfloat normal[3];
};

struct tri_tree
{
	int32_t count; 
	simplex_id id;
	struct face_struct face;
};

list *tree12 = NULL;
list *display_tree = NULL;
list *link_list = NULL;
list *boundary_list = NULL;


int menu;
int disp01=0;
int disp12=0;
int disp23=0;
int dispd=0;
int displ=0;
int dispb=0;
int disp_option=0;
int link_depth_indices[6];
int link_depth;

int cancel01;
int cancel12;
int cancel23;


int winWidth, winHeight;

GLfloat model_view_matrix[16];

GLfloat posx,posy,posz;  // view origin
GLfloat mins[3],maxs[3];

GLfloat scale;
GLfloat scalecrit0;
GLfloat scalecrit1;
GLfloat scalecrit2;
GLfloat scalecrit3;
GLfloat scalemove;
GLfloat scalemove1;
GLfloat scale_epsilon;

int rotating=0;
int startx,starty;

simplex_id selected_simplex=NULL_ID;

@ @(view.h@>=
void init_glut_window(char *vfilename);
void begin_glut(void);


