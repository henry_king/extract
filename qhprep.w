This takes a point cloud file as in the output of |enclose|
and modifies it to be acceptable input to the program
qdelaunay from the qhull project which will compute a
Delaunay triangulation.
In particular, the output should be piped to 
|qdelaunay QJ -Fx i| whose stdout will be a file of tetrahedra.

@ @<GNU license@>=

/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

@ Do it
@c

@<GNU license@>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, const char * argv[]) {
    
	int c,res;
	long n;
	float x,y,z,w;
	
	FILE *f;
	
	if(argc<2)
	{
		fprintf(stderr,"\nUsage: qhprep vertex_file | qdelaunay QJ -Fx i\n");
		exit(1);
	}
	
	f = fopen(argv[1],"r");
	
	@<count the number |n| of vertices@>
	@<strip value from all points@>

	return 0;
}


@ @<count the number |n| of vertices@>=
for(n=0; ;n++)
{
	c = fgetc(f);
	if(c!='x') ungetc(c,f);
	res = fscanf(f,"%f %f %f %f\n",&x,&y,&z,&w);
	if(res!=4) break;
}

printf("3\n%ld\n",n);
rewind(f);


@ @<strip value from all points@>=
do
{
	c = fgetc(f);
	if(c!='x') ungetc(c,f);
	res = fscanf(f,"%f %f %f %f\n",&x,&y,&z,&w);
	if(res!=4) break;
	printf("%f %f %f\n",x,y,z);
} while(1);
