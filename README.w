\def\cite#1{[{\bf #1}]}
\def\cmd#1{{\tt #1}}

\centerline{\bf Generating 3D Discrete Morse Functions from Point Data}
\medskip
\centerline{\bf Henry C. King}
\bigskip

This code is a companion to the paper \cite{KKM} by King, Knudson, and Mramor
which explains the theory behind why it works. 
It takes a simplicial complex $K$,
which is a subcomplex of a compact 3 dimensional manifold\footnote{$^1$}
{Actually $M$ does not have to be a manifold as long as $M$
minus its vertices is a manifold and the link of each vertex is connected.} $M$,
and an integer valued function $h$ on the vertices of $K$
and extends it to a discrete Morse function on $K$ in
the sense of Forman \cite{F}.
It also cancels pairs of critical $j$ and $j-1$ simplices
if they are joined by a single gradient path 
and the maximum values of $h$ on the two simplices
differ by less than a given persistence $p$.


We do not actually calculate a discrete Morse function on $K$,
but instead determine the crucial information given by a discrete Morse function.
This crucial information is the designation of some simplices as critical,
and a pairing of the remaining simplices so that each noncritical simplex is either
paired to one of its codimension one faces or is paired to a simplex
of which it is a codimension one face.

While \cite{KKM} assumes that $h$ is injective, this program does not.
The program gets around this assumption by, in effect, perturbing $h$.

For better or worse this program tries to minimize memory use at the expense of run time.
In particular, only vertices and tetrahedra are represented explicitly in memory.
Edges and triangles are referenced as being, for example, the edge between the first and
third vertices of this tetrahedron.  Thus an edge or triangle could be referenced in 
many different ways.  This can complicate the program.  A hypothetical program less worried about
optimization would be much smaller and simpler.

Parts of this program are unfinished and there are no doubt many bugs.


This particular program is set up to read in a list of vertices in $R^3$ with numerical values
and a list of tetrahedra formed from those vertices and do calculations based on that.
It could be readily modifed to have different input as desired, however the 3D visualization part
presumes of course being in $R^3$.

Practically, to use this program, do the following.
Make sure cweb, GLUT, and qhull are installed on your linux system,
as well as the usual development tools make, gcc, etc.
You could install the cweb replacement cwebx or alternatively change the makefile to
use ctangle and cweave rather than ctanglex and cweavex.
You might want TeX to read the program documentation produced.
Run \cmd{cweave README.w} if you want a more readable TeX version of this README.
Run \cmd{make} in the directory.
Generate somehow a point cloud in $R^3$ with values at each point,
represented in a file fv with one line per point, each line having four numbers separated by
spaces. For example the point $(0, 1.1, 3.4)$ with value $6.3$ would have the line
$$0\quad 1.1 \quad3.4\quad 6.3$$
Next run \cmd{./enclose fv}, which will add four more points which will not be in your
complex, but will enclose it in a tetrahedron so that in the end, your complex will
be a subcomplex of $S^3$.
Next run \cmd{./qhprep fv | qdelaunay QJ i > ft} which produces a file ft
of tetrahedra which trangulate the point cloud.
Finally, run \cmd{./extract -v fv -q ft}.
Other options for extract are:
\item{}-p persistence  The persistence amount, default = .1.
\item{}-h maxh  any two vertices with values above this will be considered to
		have equal value for persistence purposes, default = 1.
\item{}-l minh  any two vertices with values below this will be considered to
		have equal value for persistence purposes. default = 0.
\item{}-x scalex  scale all x values by dividing by this factor, default = 1.
\item{}-y scaley  scale all x values by dividing by this factor, default = 1.
\item{}-z scalez  scale all x values by dividing by this factor, default = 1.

The program produces a window using GLUT and you can interact with it as follows:
\item{}Use left mouse button to drag and rotate scene.
\item{}Use middle mouse button to select a critical simplex.
\item{}Use right mouse button to select from a popup menu:
\itemitem{}Display/Hide 0-1 gradient paths = display grad paths to a selected
		vertex or from a selected edge.  
\itemitem{}Display/Hide 1-2 gradient paths = display grad paths to a selected
		edge or from a selected triangle.  Only lines connecting the
		barycenters of the edges and triangles in the grad path are
		shown.
\itemitem{}Display/Hide 2-3 gradient paths = display grad paths to a selected
		triangle or from a selected tetrahedron.  Only lines connecting the
		barycenters of the tetrahedra and triangles in the grad path are
		shown.
\itemitem{}Display/Hide descending 2 discs = display all grad paths to a selected
		edge or from a selected triangle.  All triangles in the grad paths
		are displayed.
\itemitem{}Display/Hide link = display the link of the highest vertex of the
		selected simplex.  The lower link is shown in red.
\itemitem{}Display/Hide lower link = display the lower link of the highest vertex of 
		the selected simplex.  
\itemitem{}Cancel 0-1 = Do persistence canceling of vertices and edges, then double
		the persistence for the next canceling, so each time you click it
		you cancel more and more.
\itemitem{}Cancel 1-2 = Do persistence canceling of triangles and edges, then double
		the persistence for the next canceling.
\itemitem{}Cancel 2-3 = Do persistence canceling of triangles and tetrahedra, then 
		double the persistence for the next canceling.
\itemitem{}Compute $Z/pZ$ Betti numbers = Compute the Betti numbers for $Z/pZ$ homology
		for all primes $p \le 37$.
\itemitem{}set/reset option = display actual smooth gradient paths for the function
		$x^3-x+y^3-y+z^3-z$, they appear in red.  You can use this to compare
		the smooth paths to their combinatorial approximations.  Also after
		this is clicked on, I think I have it set up so that when descending
		2 discs are displayed, only those going between critical simplices
		are displayed.  Before clicking, all grad paths are displayed, even
		those not ending at a critical edge.
		
Keyboard actions:
\item{}space = move forward
\item{}v = move backward
\item{}b,c = rotate around some up-down axis
\item{}f,g = rotate around some up-down axis through the selected simplex and\
		also translate this simplex to the center
\item{}q = quit






\bigskip
\centerline{Bibliography}
\medskip

\item{\cite{F}} R.~Forman, {\it A User's Guide to Discrete Morse Theory}\/.

\smallskip
\item{\cite{KKM}} H.~King, K.~Knudson, and N.~Mramor, 
{\it Generating Discrete Morse Functions from Point Data}\/, Experimental Mathematics 14 (2005).

