This code takes a file of points in $R^3$ and their values
and adds four points of a tetrahedron which encloses the points.
To be specific, if there are $n$ points the input file will have 
$n$ lines.  Each line has the form "x y z v" where $x$, $y$, and $z$
are the coordinates of a point and $v$ is the value we assign to that point.
Optionally, each line may also start with the single character 'x'
to indicate the point is not actually in the complex we have in mind.
On output there will be $n+4$ lines, the last four being vertices of an enclosing
tetrahedron.  




@ Here goes

@c
@<GNU license@>@;

#include <stdio.h>
#include <stdlib.h>

int main (int argc, const char * argv[]) {
	FILE *f;
	long i,n,res;
	int c;
	float x,y,z,w;
	float minx,maxx,miny,maxy,minz,maxz,minw,maxw;
	int first_in=0;


	if(argc<2)
	{
		fprintf(stderr,"\nUsage: enclose vertexfile\n");
		exit(1);
	}
	
	f = fopen(argv[1],"r+");
		
	for(n=0; ;n++) {
		@<read in and process one point@>
	}
	@<print out min and max of each coordinate@>
	@<write enclosing four points to file f@>

	fclose(f);
	
	return 0;
}

@ @<read in and process one point@>=
c = fgetc(f);
if(c!='x') ungetc(c,f);

res = fscanf(f,"%f %f %f %f\n",&x,&y,&z,&w);
if(res!=4) break;
if(n==0) { // first line, initialize mins and maxs
	minx=maxx=x;
	miny=maxy=y;
	minz=maxz=z;
	if(c!='x') {  // point is in complex, init min and max value
		minw=maxw=w;
		first_in=1;
	}
} else {
	if(x<minx) minx=x;
	if(y<miny) miny=y;
	if(z<minz) minz=z;
	if(x>maxx) maxx=x;
	if(y>maxy) maxy=y;
	if(z>maxz) maxz=z;
	if(c!='x') { // point is in complex
		if(first_in) { // and is not the first such we have seen
			if(w<minw) minw=w;
			if(w>maxw) maxw=w;
		} else {  // first point in complex, init min and max value
			minw=maxw=w;
			first_in=1;
		}
	}
}

@ @<print out min and max of each coordinate@>=
if(res!=EOF)
{
	fprintf(stderr,"\nError on line %li\n",n);
	exit(1);
}
printf("\n%f <= x <= %f, %f <= y <= %f, %f <= z <= %f",minx,maxx,miny,maxy,minz,maxz);



@ @<write enclosing four points to file f@>=
minx = 1.01*minx-.01*maxx;
maxx = 3*maxx-2*minx;
miny = 1.01*miny-.01*maxy;
maxy = 3*maxy-2*miny;
minz = 1.01*minz-.01*maxz;
maxz = 3*maxz-2*minz;
fprintf(f,"x%f %f %f 0\n",minx,miny,minz);
fprintf(f,"x%f %f %f 0\n",maxx,miny,minz);
fprintf(f,"x%f %f %f 0\n",minx,maxy,minz);
fprintf(f,"x%f %f %f 0\n",minx,miny,maxz);

printf("\nmin = %f, max = %f\nTetrahedron:\n%li %li %li %li\n",minw,maxw,n,n+1,n+2,n+3);


@ @<GNU license@>=

/*
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
