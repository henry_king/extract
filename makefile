.SUFFIXES: .c .o .h .nw .tex
CC=gcc
CFLAGS= -c 
LDFLAGS=  -lglut -lGLU -lGL  -lXi -lXext -lX11 -lm
DEBUG= -Wall -g
NOWEBS= main.nw cancel.nw  view.nw homology.nw  simplex.nw list.nw
SOURCES=$(NOWEBS:.nw=.c)
INCLUDES=$(NOWEBS:.nw=.h)
WEAVES=$(NOWEBS:.nw=.tex)
OBJECTS=$(NOWEBS:.nw=.o)
XNOWEBS= enclose.nw qhprep.nw
XSOURCES=$(XNOWEBS:.nw=.c)
XWEAVES=$(XNOWEBS:.nw=.tex)
XOBJECTS=$(XNOWEBS:.nw=.o)

EXECUTABLE=extract

all: $(NOWEBS) $(EXECUTABLE) $(SOURCES) $(INCLUDES) $(WEAVES) $(XNOWEBS) $(XSOURCES) $(XWEAVES)


%.c: %.nw
	notangle -L -t4 $*.nw > $@ 
	
%.h: %.nw
	notangle  -R$@ $*.nw | cpif $@ 
		
%.tex: %.nw
	noweave -autodefs c -index -t4 $*.nw > $@
	
README.tex: README.nw
	noweave README.nw > $@

%.o: %.c $(INCLUDES)
	$(CC) $(DEBUG) $(CFLAGS) $< -o $@
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(DEBUG) $(OBJECTS) -o $@ $(LDFLAGS)

enclose: enclose.o
	$(CC) enclose.o -o enclose
	
qhprep: qhprep.o
	$(CC) qhprep.o -o qhprep
	
clean:
	rm -rf *.o  $(EXECUTABLE) enclose qhprep	*.c *.h *.tex *.scn *.idx *.pdf *.log *.toc
	
archive:
	tar -czvf extract.tar.gz makefile *.nw
